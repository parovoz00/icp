#include <cassert>
#include <memory>
#include <string>
#include <vector>

#define OCTREE_CHILDREN_COUNT 8

enum octree_pos
{
    TopLeftFront = 0,
    TopRightFront,
    BottomRightFront,
    BottomLeftFront,
    TopLeftBottom,
    TopRightBottom,
    BottomRightBack,
    BottomLeftBack
};

class Octree;

using point_t = std::array<float, 3>;
using point_ptr_t = std::unique_ptr<point_t>;
using bound_t = std::pair<point_t, point_t>;
using octree_ptr_t = std::unique_ptr<Octree>;
using octree_ptr_vec_t = std::vector<octree_ptr_t>;

class Octree
{
    std::unique_ptr<point_t> point_;
    bound_t bounds_;
    octree_ptr_vec_t children_;

public:
    // Default constructor
    Octree()
        : point_{ std::make_unique<point_t>(point_t{ -1, -1, -1 }) }
        , bounds_{ bound_t{} }
        , children_{ octree_ptr_vec_t{} }
    {}

    // Constructor with points
    Octree(float x, float y, float z)
        : point_{ std::make_unique<point_t>(point_t{ x, y, z }) }
        , bounds_{ bound_t{} }
        , children_{ octree_ptr_vec_t{} }
    {}

    // Constructor with bounds
    Octree(float x1, float y1, float z1, float x2, float y2, float z2)
        : point_{ nullptr }
        , bounds_{ point_t{ x1, y1, z1 }, point_t{ x2, y2, z2 } }
        , children_{ octree_ptr_vec_t{} }

    {
        for (auto i = 0; i < OCTREE_CHILDREN_COUNT; ++i)
            children_.push_back(octree_ptr_t{});
    }

    ~Octree() = default;

    void insert(float x, float y, float z);

    static enum octree_pos get_pos(float x, float y, float z, float midx,
                                   float midy, float midz)
    {
        if (x <= midx)
        {
            if (y <= midy)
            {
                if (z <= midz)
                    return TopLeftFront;
                else
                    return TopLeftBottom;
            }
            if (z <= midz)
                return BottomLeftFront;
            else
                return BottomLeftBack;
        }
        if (y <= midy)
        {
            if (z <= midz)
                return TopRightFront;
            else
                return TopRightBottom;
        }
        if (z <= midz)
            return BottomRightFront;
        else
            return BottomRightBack;
    }
};
