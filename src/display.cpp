#include "icp.hpp"

bool next_iteration = false;

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event,
                           void *nothing)
{
    (void)nothing;
    if (event.getKeySym() == "space" && event.keyDown())
        next_iteration = true;
}

void display_clouds(PointCloudT::Ptr cloud_in, PointCloudT::Ptr cloud_tr,
                    PointCloudT::Ptr cloud_icp, icp_iteration do_icp)
{
    pcl::console::TicToc time;

    pcl::visualization::PCLVisualizer viewer("ICP demo");

    // Create two vertically separated viewports
    int v1(0);
    int v2(1);
    viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
    viewer.createViewPort(0.5, 0.0, 1.0, 1.0, v2);

    // The color we will be using
    float bckgr_gray_level = 0.0; // Black
    float txt_gray_lvl = 1.0 - bckgr_gray_level;

    // Original point cloud is white
    pcl::visualization::PointCloudColorHandlerCustom<PointT> cloud_in_color_h(
        cloud_in, (int)255 * txt_gray_lvl, (int)255 * txt_gray_lvl,
        (int)255 * txt_gray_lvl);
    viewer.addPointCloud(cloud_in, cloud_in_color_h, "cloud_in_v1", v1);
    viewer.addPointCloud(cloud_in, cloud_in_color_h, "cloud_in_v2", v2);

    // Transformed point cloud is green
    pcl::visualization::PointCloudColorHandlerCustom<PointT> cloud_tr_color_h(
        cloud_tr, 20, 180, 20);
    viewer.addPointCloud(cloud_tr, cloud_tr_color_h, "cloud_tr_v1", v1);

    // ICP aligned point cloud is red
    pcl::visualization::PointCloudColorHandlerCustom<PointT> cloud_icp_color_h(
        cloud_icp, 180, 20, 20);
    viewer.addPointCloud(cloud_icp, cloud_icp_color_h, "cloud_icp_v2", v2);

    // Adding text descriptions in each viewport
    viewer.addText("White: Original point cloud\nGreen: Matrix transformed "
                   "point cloud",
                   10, 15, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl,
                   "icp_info_1", v1);
    viewer.addText("White: Original point cloud\nRed: ICP aligned point cloud",
                   10, 15, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl,
                   "icp_info_2", v2);

    // Set background color
    viewer.setBackgroundColor(bckgr_gray_level, bckgr_gray_level,
                              bckgr_gray_level, v1);
    viewer.setBackgroundColor(bckgr_gray_level, bckgr_gray_level,
                              bckgr_gray_level, v2);

    // Set camera position and orientation
    viewer.setCameraPosition(-3.68332, 2.94092, 5.71266, 0.289847, 0.921947,
                             -0.256907, 0);
    viewer.setSize(1280, 1024); // Visualiser window size

    // Register keyboard callback :
    viewer.registerKeyboardCallback(&keyboardEventOccurred, (void *)NULL);

    // Display the visualiser
    while (!viewer.wasStopped())
    {
        viewer.spinOnce();

        // The user pressed "space" :
        if (next_iteration)
        {
            do_icp(cloud_in, cloud_icp);
        }
        next_iteration = false;
    }
}
