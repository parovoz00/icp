#include "icp.hpp"

void print4x4Matrix(const Eigen::Matrix4f &matrix)
{
    printf("Rotation matrix :\n");
    printf("    | %6.3f %6.3f %6.3f | \n", matrix(0, 0), matrix(0, 1),
           matrix(0, 2));
    printf("R = | %6.3f %6.3f %6.3f | \n", matrix(1, 0), matrix(1, 1),
           matrix(1, 2));
    printf("    | %6.3f %6.3f %6.3f | \n", matrix(2, 0), matrix(2, 1),
           matrix(2, 2));
    printf("Translation vector :\n");
    printf("t = < %6.3f, %6.3f, %6.3f >\n\n", matrix(0, 3), matrix(1, 3),
           matrix(2, 3));
}

PointT compute_center_of_mass(PointCloudT::Ptr cloud)
{
    PointT center;
    for (const PointT &p : *cloud)
    {
        center.x += p.x;
        center.y += p.y;
        center.z += p.z;
    }
    center.x /= cloud->points.size();
    center.y /= cloud->points.size();
    center.z /= cloud->points.size();
    return center;
}

PointCloudT::Ptr compute_prime(PointCloudT::Ptr cloud, PointT center)
{
    PointCloudT::Ptr prime(new PointCloudT);
    // pcl::copyPointCloud(*cloud, *prime);
    prime = cloud->makeShared();
    for (PointT &p : *prime)
    {
        p.x -= center.x;
        p.y -= center.y;
        p.z -= center.z;
    }
    return prime;
}

float summs_of_products_of_points(PointCloudT::Ptr a, PointCloudT::Ptr b)
{
    if (a->empty() || b->empty())
        return 0;
    float summ = 0;
    PointCloudT::iterator ptr_a = a->points.begin();
    PointCloudT::iterator ptr_b = b->points.begin();
    while (ptr_a != a->points.end() && ptr_b != b->points.end())
    {
        float x = ptr_a->x * ptr_b->x;
        float y = ptr_a->y * ptr_b->y;
        float z = ptr_a->z * ptr_b->z;
        summ += x + y + z;
        ptr_a++;
        ptr_b++;
    }
    return summ;
}

Eigen::Vector4f power_iteration(Eigen::Matrix4f matrix,
                                int iterations) // dim(matrix) = 4
{
    srand(time(NULL));
    float x1 = rand() % matrix.size();
    float x2 = rand() % matrix.size();
    float x3 = rand() % matrix.size();
    float x4 = rand() % matrix.size();

    Eigen::Vector4f b_k = { x1, x2, x3, x4 };

    for (int i = 0; i < iterations; i++)
    {
        Eigen::Vector4f b_k1 = matrix * b_k;
        float b_k1_norm = b_k1.norm();
        for (int j = 0; j < 4; j++)
            b_k(j) = b_k1(j) / b_k1_norm;
    }
    return b_k;
}

void apply_rotation_translation(
    Eigen::Matrix3f r, Eigen::Matrix3Xf t,
    PointCloudT::Ptr cloud) // will return error in the future
{
    for (PointT &p :
         *cloud) // at one point we will have to optimize all the loop of icp
    {
        p.x = r(0, 0) * p.x + r(0, 1) * p.y + r(0, 2) * p.z + t(0);
        p.y = r(1, 0) * p.x + r(1, 1) * p.y + r(1, 2) * p.z + t(1);
        p.z = r(2, 0) * p.x + r(2, 1) * p.y + r(2, 2) * p.z + t(2);
    }
}

Eigen::Vector3f compute_translation(PointCloudT::Ptr b, PointCloudT::Ptr a)
{
    Eigen::Vector3f translation;
    translation << 0, 0, 0;
    for (std::size_t nIndex = 0; nIndex < b->points.size(); nIndex++)
    {
        translation(0) += b->points[nIndex].x - a->points[nIndex].x;
        translation(1) += b->points[nIndex].y - a->points[nIndex].y;
        translation(2) += b->points[nIndex].z - a->points[nIndex].z;
    }
    translation(0) /= b->points.size();
    translation(1) /= b->points.size();
    translation(2) /= b->points.size();
    return translation;
}

PointCloudT::Ptr copysubset(PointCloudT::iterator begin,
                            PointCloudT::iterator end)
{
    PointCloudT::Ptr new_cloud(new PointCloudT);
    while (begin != end)
    {
        PointT p;
        p.x = begin->x;
        p.y = begin->y;
        p.z = begin->z;
        new_cloud->push_back(p);
        begin++;
    }
    return new_cloud;
}

void find_alignment(PointCloudT::Ptr a, PointCloudT::Ptr b, float scaling,
                    Eigen::Matrix3f rotation, Eigen::Vector3f translate,
                    float error)
{
    PointT center_of_a = compute_center_of_mass(a);
    PointT center_of_b = compute_center_of_mass(b);

    PointCloudT::Ptr a_prime = compute_prime(a, center_of_a);
    PointCloudT::Ptr b_prime = compute_prime(b, center_of_b);

    std::size_t const a_third_size = a_prime->points.size() / 3;
    std::size_t const a_two_third_size = (a_prime->points.size() / 3) * 2;

    PointCloudT::Ptr Px = copysubset(a_prime->points.begin(),
                                     a_prime->points.begin() + a_third_size);
    PointCloudT::Ptr Py =
        copysubset(a_prime->points.begin() + a_third_size,
                   a_prime->points.begin() + a_two_third_size);
    PointCloudT::Ptr Pz = copysubset(a_prime->points.begin() + a_two_third_size,
                                     a_prime->points.end());

    std::size_t const b_third_size = b_prime->points.size() / 3;
    std::size_t const b_two_third_size = (b_prime->points.size() / 3) * 2;

    PointCloudT::Ptr Yx = copysubset(b_prime->points.begin(),
                                     b_prime->points.begin() + b_third_size);
    PointCloudT::Ptr Yy =
        copysubset(b_prime->points.begin() + b_third_size,
                   b_prime->points.begin() + b_two_third_size);
    PointCloudT::Ptr Yz = copysubset(b_prime->points.begin() + b_two_third_size,
                                     b_prime->points.end());

    float sxx = summs_of_products_of_points(Px, Yx);
    float sxy = summs_of_products_of_points(Px, Yy);
    float sxz = summs_of_products_of_points(Px, Yz);

    float syx = summs_of_products_of_points(Py, Yx);
    float syy = summs_of_products_of_points(Py, Yy);
    float syz = summs_of_products_of_points(Py, Yz);

    float szx = summs_of_products_of_points(Pz, Yx);
    float szy = summs_of_products_of_points(Pz, Yy);
    float szz = summs_of_products_of_points(Pz, Yz);

    Eigen::Matrix4f matrix;
    matrix << sxx + syy + szz, syz - szy, -sxz + szx, sxy - syx, -szy + syz,
        sxx - szz - syy, sxy + syx, sxz + szx, szx - sxz, syx + sxy,
        syy - szz - sxx, syz + szy, -syx + sxy, szx + sxz, szy + syz,
        szz - syy - sxx;

    // call power iteration
    Eigen::Vector4f lambda = power_iteration(matrix, POWER_ITERATIONS);

    float q0 = lambda(0);
    float q1 = lambda(1);
    float q2 = lambda(2);
    float q3 = lambda(3);

    Eigen::Matrix4f Qbar;
    Qbar << q0, -q1, -q2, -q3, q1, q0, q3, -q2, q2, -q3, q0, q1, q3, q2, -q1,
        q0;

    Eigen::Matrix4f Q;
    Q << q0, -q1, -q2, -q3, q1, q0, -q3, -q2, q2, -q3, q0, -q1, q3, -q2, q1, q0;

    Eigen::Matrix4f R = Qbar * Q;
    print4x4Matrix(R);
    rotation = R.block<3, 3>(1, 1);

    /*float sp_a = 0;
    float d_b = 0;
    for (PointT& q: a_prime)
    {
        d_b +=
        sp_a +=
    }
    scaling = sqrt(d/sp);*/
    scaling++;
    error++;
    Eigen::Vector3f identity_vector;
    identity_vector << 1, 1, 1;
    apply_rotation_translation(rotation, identity_vector, a_prime);
    translate = compute_translation(b_prime, a_prime);
}

PointT closest_point(PointT p, PointCloudT::Ptr a)
{
    PointT closest;
    float dist = std::numeric_limits<float>::max();
    float temp = 0;
    for (const PointT q : *a)
    {
        temp = sqrt(p.x * q.x + p.y * q.y + p.z * q.z); // remove sqrt() ?
        if (temp < dist)
        {
            dist = temp;
            closest = q;
        }
    }
    return closest;
}

PointCloudT::Ptr compute_data_association(PointCloudT::Ptr a,
                                          PointCloudT::Ptr b)
{
    PointCloudT::Ptr data(new PointCloudT);
    for (const PointT p : *b)
        data->push_back(closest_point(p, a));
    return data;
}

void icp(PointCloudT::Ptr a, PointCloudT::Ptr b)
{
    float scaling = 1;
    Eigen::Matrix3f rotation;
    rotation << 0, 0, 0, 0, 0, 0, 0, 0, 0;
    Eigen::Vector3f translate;
    translate << 0.0f, 0.0f, 0.0f;
    float error = 1.0F;

    for (int iter = 1; iter < MAX_ITER; iter++)
    {
        PointCloudT::Ptr data = compute_data_association(a, b);
        find_alignment(b, data, scaling, rotation, translate, error);
        apply_rotation_translation(rotation, translate, b);
        if (error < ICP_TRESHOLD)
            break;
    }
}
