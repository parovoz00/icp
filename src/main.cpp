#include "icp.hpp"

int main(int argc, char *argv[])
{
    // The point clouds we will be using
    PointCloudT::Ptr cloud_in(new PointCloudT); // Original point cloud
    PointCloudT::Ptr cloud_tr(new PointCloudT); // Transformed point cloud
    PointCloudT::Ptr cloud_icp(new PointCloudT); // ICP output point cloud

    // Checking program arguments
    if (argc < 2)
    {
        printf("Usage :\n");
        printf("\t\t%s file.ply\n", argv[0]);
        PCL_ERROR("Provide one ply file.\n");
        return (-1);
    }

    if (std::string(argv[1]).find(".ply") != std::string::npos)
    {
        if (pcl::io::loadPLYFile(argv[1], *cloud_in) < 0)
        {
            PCL_ERROR("Error loading cloud %s.\n", argv[1]);
            return (-1);
        }
        if (argc == 3)
        {
            if (pcl::io::loadPLYFile(argv[2], *cloud_icp) < 0)
            {
                PCL_ERROR("Error loading cloud %s.\n", argv[1]);
                return (-1);
            }
        }
        else
        {
            // Defining a rotation matrix and translation vector
            Eigen::Matrix4d transformation_matrix = Eigen::Matrix4d::Identity();

            // A rotation matrix (see
            // https://en.wikipedia.org/wiki/Rotation_matrix)
            double theta = M_PI / 8; // The angle of rotation in radians
            transformation_matrix(0, 0) = std::cos(theta);
            transformation_matrix(0, 1) = -sin(theta);
            transformation_matrix(1, 0) = sin(theta);
            transformation_matrix(1, 1) = std::cos(theta);

            // A translation on Z axis (0.4 meters)
            transformation_matrix(2, 3) = 0.4;

            // Executing the transformation
            pcl::transformPointCloud(*cloud_in, *cloud_icp,
                                     transformation_matrix);
        }
    }
    else
    {
        cloud_in = parse_cloud(argv[1]);
        cloud_icp = parse_cloud(argv[2]);
    }
    *cloud_tr = *cloud_icp; // We backup cloud_icp into cloud_tr for later use

    display_clouds(cloud_in, cloud_tr, cloud_icp, icp);
}
