#include "octree.hpp"

void Octree::insert(float x, float y, float z)
{
    float midx = (bounds_.first[0] + bounds_.second[0]) / 2.0;
    float midy = (bounds_.first[1] + bounds_.second[1]) / 2.0;
    float midz = (bounds_.first[2] + bounds_.second[2]) / 2.0;

    enum octree_pos pos = Octree::get_pos(x, y, z, midx, midy, midz);

    if (children_[pos]->point_ == nullptr)
    {
        children_[pos]->insert(x, y, z);
        return;
    }

    if ((*children_[pos]->point_)[0] == -1)
    {
        (*children_[pos]->point_)[0] = x;
        (*children_[pos]->point_)[1] = y;
        (*children_[pos]->point_)[2] = z;
        return;
    }

    point_t tmp_point = *children_[pos]->point_;
    point_t top_left_front = bounds_.first;
    point_t bottom_right_back = bounds_.second;

    switch (pos)
    {
    case TopLeftFront:
        children_[pos]->bounds_.first[0] = top_left_front[0];
        children_[pos]->bounds_.first[1] = top_left_front[1];
        children_[pos]->bounds_.first[2] = top_left_front[2];

        children_[pos]->bounds_.second[0] = midx;
        children_[pos]->bounds_.second[1] = midy;
        children_[pos]->bounds_.second[2] = midz;
        break;

    case TopRightFront:
        children_[pos]->bounds_.first[0] = midx + 1;
        children_[pos]->bounds_.first[1] = top_left_front[1];
        children_[pos]->bounds_.first[2] = top_left_front[2];

        children_[pos]->bounds_.second[0] = bottom_right_back[0];
        children_[pos]->bounds_.second[1] = midy;
        children_[pos]->bounds_.second[2] = midz;
        break;

    case BottomRightFront:
        children_[pos]->bounds_.first[0] = midx + 1;
        children_[pos]->bounds_.first[1] = midy + 1;
        children_[pos]->bounds_.first[2] = top_left_front[2];

        children_[pos]->bounds_.second[0] = bottom_right_back[0];
        children_[pos]->bounds_.second[1] = bottom_right_back[1];
        children_[pos]->bounds_.second[2] = midz;
        break;

    case BottomLeftFront:
        children_[pos]->bounds_.first[0] = top_left_front[0];
        children_[pos]->bounds_.first[1] = midy + 1;
        children_[pos]->bounds_.first[2] = top_left_front[2];

        children_[pos]->bounds_.second[0] = midx;
        children_[pos]->bounds_.second[1] = bottom_right_back[1];
        children_[pos]->bounds_.second[2] = midz;
        break;

    case TopLeftBottom:
        children_[pos]->bounds_.first[0] = top_left_front[0];
        children_[pos]->bounds_.first[1] = top_left_front[1];
        children_[pos]->bounds_.first[2] = midz + 1;

        children_[pos]->bounds_.second[0] = midx;
        children_[pos]->bounds_.second[1] = midy;
        children_[pos]->bounds_.second[2] = bottom_right_back[2];
        break;

    case TopRightBottom:
        children_[pos]->bounds_.first[0] = midx + 1;
        children_[pos]->bounds_.first[1] = top_left_front[1];
        children_[pos]->bounds_.first[2] = midz + 1;

        children_[pos]->bounds_.second[0] = bottom_right_back[0];
        children_[pos]->bounds_.second[1] = midy;
        children_[pos]->bounds_.second[2] = bottom_right_back[2];
        break;

    case BottomRightBack:
        children_[pos]->bounds_.first[0] = midx + 1;
        children_[pos]->bounds_.first[1] = midy + 1;
        children_[pos]->bounds_.first[2] = midz + 1;

        children_[pos]->bounds_.second[0] = bottom_right_back[0];
        children_[pos]->bounds_.second[1] = bottom_right_back[1];
        children_[pos]->bounds_.second[2] = bottom_right_back[2];
        break;

    case BottomLeftBack:
        children_[pos]->bounds_.first[0] = top_left_front[0];
        children_[pos]->bounds_.first[1] = midy + 1;
        children_[pos]->bounds_.first[2] = midz + 1;

        children_[pos]->bounds_.second[0] = midx;
        children_[pos]->bounds_.second[1] = bottom_right_back[1];
        children_[pos]->bounds_.second[2] = bottom_right_back[2];
        break;
    }

    children_[pos]->insert(tmp_point[0], tmp_point[1], tmp_point[2]);
    children_[pos]->insert(x, y, z);
}
