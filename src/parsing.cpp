#include "icp.hpp"

PointCloudT::Ptr parse_cloud(std::string filename)
{
    PointCloudT::Ptr cloud(new PointCloudT);
    std::cout << "Begin Loading Model" << std::endl;
    std::ifstream f(filename);
    std::string line;

    if (!f)
    {
        std::cout << "ERROR: failed to open file: " << filename << endl;
        return nullptr;
    }

    std::getline(f, line);
    while (std::getline(f, line))
    {
        std::string item;
        std::stringstream sstr(line);
        PointT point;
        std::getline(sstr, item, ',');
        point.x = std::stof(item);
        std::getline(sstr, item, ',');
        point.y = std::stof(item);
        std::getline(sstr, item, ',');
        point.z = std::stof(item);

        cloud->push_back(point);
    }

    f.close();

    std::cout << "Loaded cloud with " << cloud->points.size() << " points."
              << std::endl;

    return cloud;
}
